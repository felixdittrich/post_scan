denoiser:
Autoencoder / Denoising Autoencoder: https://www.jeremyjordan.me/autoencoders/
Fully-convolutional-networks: https://jianchao-li.github.io/post/understanding-fully-convolutional-networks/

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

evaluator:
Convolutional neural network (CNN): https://cs231n.github.io/convolutional-networks/

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

handy_helper: (no Deep Learning only Computer Vision)
Gaussian-Blur: https://www.pixelstech.net/article/1353768112-Gaussian-Blur-Algorithm
Thresholding: https://robotacademy.net.au/lesson/image-thresholding/
Contours: https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_contours/py_table_of_contents_contours/py_table_of_contents_contours.html


if any trouble: felixdittrich92@gmail.com






