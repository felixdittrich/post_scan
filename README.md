[![license](https://img.shields.io/github/license/mashape/apistatus.svg)](LICENSE)
[![https://www.singularity-hub.org/static/img/hosted-singularity--hub-%23e32929.svg](https://www.singularity-hub.org/static/img/hosted-singularity--hub-%23e32929.svg)](https://singularity.gwdg.de/collections/8) 

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Document utilities](#introduction)
	- [Requirements](#requirements)
	- [Installation](#installation)
  - [Data](#data)
  - [Usage](#usage)
	  - [predict quality of document images](#predict-quality-of-document-images)
	  - [denoising image](#denoising-image)
  - [Helper](#helper)
  - [Custom Training](#custom-training)

<!-- /TOC -->

## Introduction

### Document utilities 

Implementation of a: 

1. `CNN` to classify the document quality (reading and ocr usage) 

2. `Autoencoder` to denoise scanned document images (100-300dpi)

Helper:

1. a helper util to prepare a handy image for OCR, Autoencoder etc.

---

## Requirements

- Linux (Ubuntu 20.04)
- Anaconda or miniconda
- Nvidia GPU >= 8GB VRAM (recommended for custom training)

## Installation

### local (recommended)

1. clone the repository
```
git clone URL
```
2. install requirements

- install Anaconda and create an environment
```
$ cd ./post_scan
$ conda env create --name <env> -f environment.yml
$ conda activate <env>
```

### Singularity Container

1. install singularity [Singularity installation](https://gitlab.gwdg.de/mpg-singularity/mpg-singularity/-/wikis/Singularity-unter-Linux-installieren) or `pip3 install singularity`

2. download [Singularity container](https://singularity.gwdg.de/collections)
- Collection: document-scanner
- post_scan contains: predict quality of document images, denoising images, helper 

3. run container and read help section 
```
$ singularity run-help post_scan.sif

$ singularity run --bind PATH TO FOLDER post_scan.sif  (bind a folder or file with the container)
```

## Data
if you are interested in getting the raw data contact me:
[mailto](mailto:felixdittrich92@gmail.com)

- Denoiser: 365 items
```
denoising
        |
        |_train: 1.png, 2.png, ..
        |           
        |_train_cleaned: 1.png, 2.png, ..
        |
        |_test: 1.png, 2.png, ..
```

- Evaluator: 275 items
```
Scans
    |
    |_best: 1.jpg, 2.jpg, ..
    |
    |_worst: 1.jpg, 2.jpg, ..
```

## Usage

### predict quality of document images

```
python3 predict_quality.py 
--path=PATH TO SINGLE IMAGE OR A FOLDER WITH IMAGES (NO SUBFOLDER SUPPORT)
```
<p float="left">
  <img allign="left" src="images/original.jpg" width="400px"/> 
  <img allign="right" src="images/autoencoder_result.jpg" width="400px"/> 
</p>

```
Image name : original.jpg                                    Image name : autoencoder_result.jpg 
 Predicted class : not good                                   Predicted class : not good         
 Score : 0.007537	                                      Score : 0.235914                   
```
<p float="left">
  <img allign="left" src="images/output.jpg" width="400px"/> 
  <img allign="right" src="images/lineart_300dpi.jpg" width="400px"/> 
</p>

```
 Image name : output.jpg                                    Image name : lineart_300dpi.jpg
  Predicted class : good                                     Predicted class : good
  Score : 0.728959                                           Score : 0.999801 
```
### denoising image 
```
python3 denoising_image.py 
--input=PATH TO SINGLE IMAGE 
--output=NAME OF THE OUTPUT FILE (Example: "out.png")
--quality=QUALITY FOR THE OUTPUT IMAGE (0-100) (Optional)
--res_width=THE WIDTH SIZE FOR THE OUTPUT IMAGE (ASPECT RATIO IS CALCULATED)(Optional)
```
#### ORIGINAL / RESULT:
<p float="left">
  <img allign="left" src="images/color_300dpi.jpg" width="400px"/>
  <img allign="right" src="images/output.jpg" width="400px"/>
</p>

## Helper
- NOTE: the image needs a clear background / four edges must be recognizable
- rotate the image correctly and crop the relevant part / add sharpening with --sharp=True
```
python3 handy_helper/helper.py 
--input=PATH TO SINGLE IMAGE 
--output=NAME OF THE OUTPUT FILE (Example: "out.png")
--sharp=True (Optional)
--quality=QUALITY FOR THE OUTPUT IMAGE (0-100) (Optional)
--res_width=THE WIDTH SIZE FOR THE OUTPUT IMAGE (ASPECT RATIO IS CALCULATED)(Optional)
```
#### ORIGINAL / RESULT

<p float="left">
  <img allign="left" src="images/handy.jpg" width="400px"/> 
  <img allign="right" src="images/handy_out.jpg" width="380px"/> 
</p>

## Custom Training
- NOTE: If you want to train your own data, look in the data section in which structure the data must be available

### Evaluator
```
evaluator/Dataset.py -> Line 16 -> path to your Dataset
evaluator/CNN.py -> Linie 141/181 -> change save path 
cd post_scan/evaluator
python3 CNN.py
predict_quality.py -> Line 18 -> change to your model stored path
```
### Denoiser
```
denoiser/Autoencoder.py -> Line 30/31 -> path to your Dataset
denoiser/Autoencoder.py -> Line 124 / 158 -> change save path
cd post_scan/denoiser
python3 Autoencoder.py
denoising_image.py -> Line 25 -> change to your model stored path
```